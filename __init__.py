from math import floor

import leds
import st3m.run
from ctx import Context
from st3m import InputState
from st3m import logging
from st3m.application import Application, ApplicationContext
from st3m.input import InputController
from st3m.ui import colours
from st3m.utils import tau

log = logging.Log("ZeitTimer", level=logging.INFO)
log.info("ZeitTimer started")


class ZeitTimerApp(Application):
    def __init__(self, app_ctx: ApplicationContext):
        super().__init__(app_ctx)
        self.input = InputController()
        self.time_left = 5
        self.running = False
        self.alarmed = False
        self.bg_colour = colours.hsv_to_rgb(62, 0.3, 1)

    def think(self, ins: InputState, delta_ms: int) -> None:
        super().think(ins, delta_ms)

        if self.running:
            self.time_left -= delta_ms / 1000 / 60

        # Toggle timer running
        if self.input.buttons.app.middle.pressed:
            if self.running:
                self.running = False
                # self.bg_colour = colours.hsv_to_rgb(62, 0.3, 0.5)
            else:
                self.running = True
                self.alarmed = False
                # self.bg_colour = colours.hsv_to_rgb(62, 0.3, 1)

        # Hold down left to increase time
        if self.input.buttons.app.left.down:
            self.time_left -= delta_ms / 300

        # Hold down right to decrease time
        if self.input.buttons.app.right.down:
            self.time_left += delta_ms / 300

        if self.time_left < 0:
            self.time_left = 0
        elif self.time_left > 60:
            self.time_left = 60

        if self.time_left == 0:
            self.running = False
            leds.set_all_rgb(0, 0.5, 0)
        else:
            for led in range(40):
                if self.time_left > led / 40 * 60 - 2:
                    leds.set_rgb(led, 1, 0, 0)
                else:
                    if self.running:
                        leds.set_rgb(led, *colours.hsv_to_rgb(62, 0.3, 0.8))
                    else:
                        leds.set_rgb(led, *colours.hsv_to_rgb(62, 0.3, 0.2))

        if not self.alarmed and self.running and self.time_left <= 0:
            log.info("Alarm! (TODO)")
            self.alarmed = True

    def on_enter(self, vm):
        leds.set_all_rgb(0, 0, 0)
        leds.set_slew_rate(100)
        leds.update()
        leds.set_auto_update(True)

    def draw(self, ctx: Context) -> None:
        ctx.rgb(*self.bg_colour)
        ctx.rectangle(-120, -120, 240, 240)
        ctx.fill()

        if self.time_left > 0:
            # Draw red sector
            ctx.begin_path()
            ctx.save()
            ctx.rgb(1, 0, 0)
            ctx.move_to(0, 0)
            ctx.line_to(0, -122)
            ctx.arc(0, 0, 122, -tau / 4, self.time_left / 60 * tau - tau / 4, 0)
            ctx.line_to(0, 0)
            ctx.close_path()
            ctx.fill()
            ctx.restore()

            # Draw viisari lines
            ctx.begin_path()
            ctx.save()
            ctx.line_width = 2.0
            ctx.rgb(0, 0, 0)
            ctx.move_to(0, 0)
            ctx.line_to(0, -122)
            ctx.arc(0, 0, 122, -tau / 4, self.time_left / 60 * tau - tau / 4, 0)
            ctx.line_to(0, 0)
            ctx.close_path()
            ctx.rgb(0, 0, 0)
            ctx.stroke()
            ctx.restore()

        ctx.rgb(0, 0, 0)

        # Draw small tick marks for every minute
        for i in range(60):
            ctx.save()
            ctx.rotate(tau / 60 * i)
            ctx.move_to(0, -117)
            ctx.line_to(0, -121)
            ctx.stroke()
            ctx.restore()

        # Draw longer tick marks for every 5 minutes
        for i in range(12):
            ctx.save()
            ctx.rotate(tau / 12 * i)
            ctx.move_to(0, -105)
            ctx.line_to(0, -121)
            ctx.stroke()
            ctx.restore()

        # Draw minutes and seconds
        if self.time_left > 0:
            ctx.font_size = 64
            ctx.text_align = ctx.RIGHT
            ctx.text_baseline = ctx.MIDDLE
            ctx.font = "Comic Mono"
            ctx.move_to(-15, 0)
            ctx.rgb(0, 0, 0)
            ctx.save()
            ctx.text(f"{floor(self.time_left):2d}")
            ctx.font_size = 50
            ctx.move_to(65, 0)
            ctx.text(f"{floor(self.time_left % 1 * 60):02d}")
            ctx.restore()
        else:
            ctx.move_to(0, 0)
            ctx.text_align = ctx.CENTER
            ctx.text_baseline = ctx.MIDDLE
            ctx.font = "Comic Mono"
            ctx.text("Time's up!")


if __name__ == '__main__':
    st3m.run.run_app(ZeitTimerApp)
