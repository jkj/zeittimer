# ZeitTimer

A visual 60 minute timer app for children and adults.

Models the timer used in schools and daycare centers.
The circle signifies a full hour and there is a shrinking red sector showing how much time is left.
